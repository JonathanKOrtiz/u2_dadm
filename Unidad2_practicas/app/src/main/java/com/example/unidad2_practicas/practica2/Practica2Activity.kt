package com.example.unidad2_practicas.practica2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.unidad2_practicas.R
import com.example.unidad2_practicas.practica1.DuracionSelectorActivity
import kotlinx.android.synthetic.main.activity_main.*

const val DURATION_RESULT = 3000
class Practica2Activity : AppCompatActivity() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == RESULT_OK) {
            when (requestCode){
                DURATION_RESULT ->{
                    if(data != null){

                       val minutes = data.getIntExtra("DURATION",-1)
                        if (minutes>=60){
                           btnpractica2.text = resources.getQuantityString(R.plurals.pluralsHours, minutes/60, minutes/60)
                             }else{
                                 btnpractica2.text = "$minutes minutes"
                        }

                    }

                }
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnpractica1.setOnClickListener{
            val intent = Intent(this, DuracionSelectorActivity::class.java)
            startActivityForResult(intent, DURATION_RESULT)
        }
    }
}