package com.example.unidad2_practicas.practica1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.unidad2_practicas.R
import com.example.unidad2_practicas.practica2.DurationAdapter
import kotlinx.android.synthetic.main.activity_duracion_selector.*


class DuracionSelectorActivity : AppCompatActivity() {

    var durationList = mutableListOf<Int>()
    private val adapter by lazy {
        DurationAdapter{ duration ->

            intent.putExtra("DURATION",duration)
            setResult(RESULT_OK, intent)
            finish()

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_duracion_selector)

        durationList = mutableListOf(5, 10,15,20,25,30,60,120)
        rvDuration.adapter = adapter
        adapter.setList(durationList)

    }
}