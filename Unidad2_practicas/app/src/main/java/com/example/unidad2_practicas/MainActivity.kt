package com.example.unidad2_practicas

import Practica1.Practica1Activity
import Practica3.Practica3Activity
import Practica4.Practica4Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.unidad2_practicas.practica2.Practica2Activity
import com.example.unidad2_practicas.practica6.practica6Activity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPractica1.setOnClickListener {
            val intent = Intent(this, Practica1Activity::class.java)
            startActivity(intent)
        }
        btnPract2.setOnClickListener {
            val intent = Intent(this, Practica2Activity::class.java)
            startActivity(intent)
        }
        btnPract3.setOnClickListener {
            val intent = Intent(this, Practica3Activity::class.java)
            startActivity(intent)
        }
        btnPract4.setOnClickListener {
            val intent = Intent(this, Practica4Activity::class.java)
            startActivity(intent)
        }
        btnPract5.setOnClickListener {
            val intent = Intent(this, Practica5Activity::class.java)
            startActivity(intent)
        }
        btnPract6.setOnClickListener {
            val intent = Intent(this, practica6Activity::class.java)
            startActivity(intent)
        }
        btnPI.setOnClickListener {
            val intent = Intent(this, PIntegradoraActivity::class.java)
            startActivity(intent)
        }
    }
}