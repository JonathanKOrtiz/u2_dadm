package com.example.unidad2_practicas.practica6

import org.json.JSONArray
import org.json.JSONObject

class citiesandtimezones {
    var cities = arrayListOf<CitiesTimezones>()

    constructor()

    constructor(jsonArray: JSONArray){
        for (i in 0 until  jsonArray.length()){
            val data = jsonArray.getJSONObject(i)
            cities.add(CitiesTimezones(data))
        }

    }
    class CitiesTimezones (jsonObject: JSONObject){
        var name: String =""
        var country: String =""
        var timezones: String=""

        init{
            name = jsonObject.getString("Nombre")
            country = jsonObject.getString("Ciudad")
            timezones = jsonObject.getString("TimeZoneName")
        }

    }
}