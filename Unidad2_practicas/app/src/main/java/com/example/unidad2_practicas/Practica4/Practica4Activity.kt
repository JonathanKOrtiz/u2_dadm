package Practica4

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class Practica4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica4)

        btndialogo.setOnClickListener {

            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage("ESTO ES UN CUADRO DE DIALOGO")
                .setTitle("HOLA")

                // .setCancelable(false) //para salir del cuadro de dialogo
                .setPositiveButton("SI") { dialog, which ->
                    Toast.makeText(this, "AMONOS", Toast.LENGTH_SHORT).show()
                }
                .setNeutralButton("NO LO SE") { dialog, which ->
                    Toast.makeText(this, "NOPI", Toast.LENGTH_SHORT).show()
                }
                .setNegativeButton("NO") { dialog, which ->
                    Toast.makeText(this, "nel nel", Toast.LENGTH_SHORT).show()

                }

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        btn2.setOnClickListener {
            val color = arrayOf("red", "blue", "green", "yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                .setTitle("Escoje un color")
                .setItems(color) { dialog, which ->
                    when (which) {
                        0 -> Toast.makeText(this, "red", Toast.LENGTH_SHORT).show()

                        1 -> Toast.makeText(this, "blue", Toast.LENGTH_SHORT).show()
                        2 -> Toast.makeText(this, "green", Toast.LENGTH_SHORT).show()
                        3 -> Toast.makeText(this, "yellow", Toast.LENGTH_SHORT).show()
                    }

                }
            val dialog = builder.create()
            dialog.show()
        }
    }
}
