package com.example.unidad2_practicas.Practicaintegradora

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.unidad2_practicas.R
import com.example.unidad2_practicas.Practicaintegradora.CandTViewHolder
import com.example.unidad2_practicas.Practicaintegradora.CandT
import com.example.unidad2_practicas.practica6.citiesandtimezones
import kotlinx.android.synthetic.main.iteams_cities_timezones.view.*
import java.text.SimpleDateFormat
import java.util.*

class cAdapter(private val listener: (CandT.Clima)-> Unit):
    RecyclerView.Adapter<ClimaViewHolder> (){
    private var list = mutableListOf<CandT.Clima>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ClimaViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_clima,parent, false)
        return ClimaViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ClimaViewHolder, position: Int) {
        holder.setData(list[position],listener)
    }
    override fun getItemCount(): Int {
        return  list.size
    }
    fun setList(list: List<CandT.Clima>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}
class ClimaViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
    fun  setData(item: CandT.Clima, listener: (CandT.Clima)-> Unit){
        itemView.apply {

            tvCC.text = "${item.name}/${item.country}"

            setOnClickListener{ listener.invoke(item)}

        }

    }

}
fun getDayOfTheWeek(date: Date): String{
    val format = SimpleDateFormat("EEEE, d", Locale.getDefault())
    return format.format(date)
}
fun getWeatherIcon (name: String): Int{
    return when (name){
        "clear-day" -> R.drawable.wic_clear_day
        "clear-night" -> R.drawable.wic_clear_night
        "rain" -> R.drawable.wic_rain
        "snow" -> R.drawable.wic_snow
        "wind" -> R.drawable.wic_wind
        "fog" -> R.drawable.wic_fog
        "sleet" -> R.drawable.wic_sleet
        "cloudy" -> R.drawable.wic_cloudy
        "cloudy-cloudy-day" -> R.drawable.wic_partly_cloudy_day
        "partly-cloudy-night" -> R.drawable.wic_partly_cloudy_night
        "hail" -> R.drawable.wic_hail
        "thunderstorm" -> R.drawable.wic_thunderstorm
        "tornado" -> R.drawable.wic_tornado
        else -> R.drawable.no_error
    }
}
