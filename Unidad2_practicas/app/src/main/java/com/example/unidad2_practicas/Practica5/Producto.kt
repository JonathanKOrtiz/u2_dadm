package com.example.unidad2_practicas.Practica5

import android.widget.TextView

class Producto(val nombre: TextView, val precio: Double, val descripcion: String, val imagen: Int)
