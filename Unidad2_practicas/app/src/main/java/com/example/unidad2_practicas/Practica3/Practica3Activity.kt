package Practica3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.R
import com.example.unida2_practicas.R
import kotlinx.android.synthetic.main.activity_practica3.*

class Practica3Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica3)

        btnShow.setOnClickListener {
            //VALIDAR DATOS
            val bundle = Bundle()
            val intent = Intent(this, P3Activity::class.java)
            bundle.putString("Name", etName.text.toString())
            bundle.putString("AGE", etAge.text.toString())
            bundle.putString("Lastname", etLastname.text.toString())
            bundle.putString("Salary", edSalary.text.toString())

            if (etName.text.isNotEmpty() && etLastname.text.isNotEmpty() &&
                etAge.text.isNotEmpty() && edSalary.text.isNotEmpty()
            ) {
                btndatos.setOnClickListener {
                    val bundle = Bundle()
                    val intent = Intent(this, P3Activity::class.java)

                    bundle.putString("Nombre", etName.text.toString())
                    bundle.putString("Apellidos", etLastname.text.toString())
                    bundle.putInt("Edad", etAge.text.toString().toInt())
                    bundle.putInt("Salario", etSalary.text.toString().toInt())
                    bundle.putString("Genero", genero)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            } else {
                Toast.makeText(this, "Por favor rellene todos los datos", Toast.LENGTH_SHORT).show()
            }

        }
        rgGender.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbMale -> {
                    gender = com.tec.practicas_unidad2
                }
                R.id.rbFemale -> {

                }
                R.id.rbNB -> {

                }
                R.id.rbNI -> {

                }

            }
        }
    }
}
