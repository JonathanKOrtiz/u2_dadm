package Practica1

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.unida2_practicas.R
import kotlinx.android.synthetic.main.activity_practica1.*

class Practica1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1)



            btnCall.setOnClickListener {
                val telephone = etPhone.text.toString()
                if (validateCallPermissions()) { //YA TENEMOS PERMISO
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$telephone"))
                    startActivity(intent)
                } else {//nO TENEMOS PERMISO
                    val permission = arrayOf(Manifest.permission.CALL_PHONE)
                    ActivityCompat.requestPermissions(this, permission, 101)
                }
            }
        }

        private fun validateCallPermissions(): Boolean {
            return ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CALL_PHONE
            ) == PackageManager.PERMISSION_GRANTED

            false
        }

    }
