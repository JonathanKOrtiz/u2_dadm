package com.example.unidad2_practicas.practica6

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.unidad2_practicas.R
import kotlinx.android.synthetic.main.activity_practica6.*
import org.json.JSONArray
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter

class practica6Activity : AppCompatActivity() {

    private val adapter by lazy {
        CiudadAdapter { selectedCity ->

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica6)

        rvtime.adapter = adapter

        val inputS = resources.openRawResource(R.raw.cities_and_timezones)
        val writer = StringWriter()
        val buffer = CharArray(1024)
        inputS.use { input ->
            val reader = BufferedReader(InputStreamReader(input, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        }
        val jsonArray = JSONArray(writer.toString())
        val citieslist = citiesandtimezones(jsonArray)

        adapter.setList(citieslist.cities)
    }
}