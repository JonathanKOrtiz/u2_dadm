package com.example.unidad2_practicas.practica6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.unidad2_practicas.R
import kotlinx.android.synthetic.main.iteams_cities_timezones.view.*

class CiudadAdapter(private val listener: (citiesandtimezones.CitiesTimezones)-> Unit): RecyclerView.Adapter<CitiesTimeZonesViewHolder> (){
    private var list = mutableListOf<citiesandtimezones.CitiesTimezones>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesTimeZonesViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.iteams_cities_timezones,parent, false)
        return CitiesTimeZonesViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CitiesTimeZonesViewHolder, position: Int) {
        holder.setData(list[position],listener)
    }

    override fun getItemCount(): Int {
        return  list.size
    }
    fun setList(list: List<citiesandtimezones.CitiesTimezones>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}
class CitiesTimeZonesViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
    fun  setData(item: citiesandtimezones.CitiesTimezones, listener: (citiesandtimezones.CitiesTimezones)-> Unit){
        itemView.apply {

            tvCC.text = "${item.name}/${item.country}"

            setOnClickListener{ listener.invoke(item)}

        }

    }

}