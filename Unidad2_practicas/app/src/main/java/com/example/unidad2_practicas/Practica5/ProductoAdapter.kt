package com.example.unidad2_practicas.Practica5

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.unidad2_practicas.R
import kotlinx.android.synthetic.main.item_producto.view.*

class ProductoAdapter(private val mContext: Context, private val lista: List<Producto>) :
    ArrayAdapter<Producto>(mContext, 0, lista) {

    var infor: MutableList<Producto> = ArrayList() //Lista de objeto participante
    lateinit var contexto: Context//sirve para definir despues algo que aun no esta disponible

    fun ProductoAdapter(lista: MutableList<Producto>, contexto: Context) {
        this.infor = lista
        this.contexto = contexto
    }

    override fun onBindViewHolder(//Enlazar con los datos
        holder: ProductoAdapter.ViewHolder,
        position: Int
    ) {
        val inf = infor[position]
        holder.bind(inf, contexto)
    }

    override fun getItemCount(): Int { //Cantidad de elementos
        return infor.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductoAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)//carga un inflador
        return ViewHolder( //devuelve una lista
            layoutInflater.inflate(
                R.layout.item_producto,
                parent,
                false
            )
        )

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val nombre = view.findViewById(R.id.nombre) as TextView //elemento de la interfaz
        private val precio = view.findViewById(R.id.precio) as TextView
        private val imagen = view.findViewById(R.id.imgT) as ImageView

        fun bind(pro: Producto, Context: Context) {
            nombre.text = pro.nombre//el nombre del participant que se esta recibiendo
            precio.text = pro.precio//el aepellido del participant que se esta recibiendo
            imagen.setImageResource(pro.imagen!!)
            itemView.setOnClickListener {
                Toast.makeText(
                    Context,
                    pro.nombre,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
