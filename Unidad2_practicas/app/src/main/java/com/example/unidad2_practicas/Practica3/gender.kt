package Practica3

enum class gender {
    MALE, FEMALE, NOT_BINARY, NOT_IDENTYFIED;

    enum class Genero(var namee: String) {
        MALE("Hombre"),
        FEMALE("Mujer"),
        NOT_BINARY("No binario"),
        NOT_IDENTYFIED("Sin especificar")
    }
}
