package com.example.unidad2_practicas.Practica5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.unidad2_practicas.R
import kotlinx.android.synthetic.main.item_producto.*

class Practica5Activity : AppCompatActivity() {


    private val adaptador: ProductoAdapter = ProductoAdapter()//es el adaptador que se creo


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica5)
        val rvIn = findViewById(R.id.rvIn) as RecyclerView//donde se preparo la interfaz visual
        rvIn.setHasFixedSize(true) //es para un tamaño fijo
        rvIn.layoutManager =
            LinearLayoutManager(this)//es para que aparezcan los datos uno encima de otro
        adaptador.ProductoAdapter(
            crearListainformacion(),
            this
        )//adaptador apartir de la listainformacion
        rvIn.adapter = adaptador

    }

    private fun crearListainformacion(): MutableList<Producto> { //devuelve una lista de información
        val infor: MutableList<Producto> = arrayListOf()
        infor.add(
            Producto(
                "Fresas",
                10.0,
                "Fresas frescas y dulces",
                R.drawable.fresa
            )
        )
        return infor
    }
}
